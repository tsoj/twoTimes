### Building

Set up cmake:
```bash
./setup_cmake.sh
```

Compile:
```bash
make -j
```

Run:
```bash
./twoTimes
```