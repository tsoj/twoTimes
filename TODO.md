### TODO

Try:

- more/fewer out channels for conv
- random number of out channels
- Random number of batches
- instead of dilution, filter input and make it smaller
- fewer number of neurons on fc layers
- 2 instead of 3 fc layers

- voting instead of average
- interpolation instead of nearest
- bigger batch size
- indices only with more models
