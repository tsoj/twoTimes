#pragma once

#include "expected_csv_header.hpp"
#include "utils.hpp"

#include <filesystem>
#include <iostream>
#include <limits>
#include <string>
#include <torch/torch.h>
#include <vector>

using namespace torch::indexing;

enum class LandClass
{
    unknown = -1,
    prairie_temporaire = 0,
    ble = 1,
    pre = 2,
    feuillus = 3,
    tournesol = 4,
    mais_ensillage = 5,
    jachere = 6,
    bati_dense = 7,
    bati_diffus = 8,
    friche = 9,
    resineux = 10,
    sorgho = 11,
    pois = 12,
    orge = 13,
    bati_indu = 14,
    soja = 15,
    eau = 16,
    eucalyptus = 17,
    colza = 18,
    lac = 19,
    peupliers = 20,
    mais = 21,
    graviere = 22,
    surface_minerale = 23
};
constexpr int num_classes = 24;
constexpr int num_channels = 3;
constexpr int num_time_steps = 46;

enum class Channel
{
    nir,
    red,
    green
};
constexpr auto channels = {Channel::nir, Channel::red, Channel::green};

std::ostream& operator<<(std::ostream& stream, const Channel& channel)
{
    switch (channel)
    {
    case Channel::nir: stream << "nir"; break;
    case Channel::red: stream << "red"; break;
    case Channel::green: stream << "green"; break;
    }
    return stream;
}

class Pixel
{
  public:
    float& operator[](const Channel& channel) { return m_data[static_cast<int>(channel)]; }

    const float& operator[](const Channel& channel) const { return m_data[static_cast<int>(channel)]; }

  private:
    std::array<float, 3> m_data;
};

struct DataEntry
{
    int id;
    LandClass target;
    std::array<Pixel, num_time_steps> time_series;
};

torch::Tensor toTensor(const std::array<Pixel, num_time_steps>& time_series)
{
    auto result = torch::zeros({num_channels, num_time_steps});
    for (int i = 0; i < num_time_steps; i++)
    {
        for (const auto channel : channels)
        {
            result[static_cast<int>(channel)][i] = time_series.at(i)[channel];
        }
    }
    return result;
}

std::ostream& operator<<(std::ostream& stream, const DataEntry& entry)
{
    stream << "ID: " << entry.id << ", class: " << static_cast<int>(entry.target);
    for (const auto channel : channels)
    {
        stream << "\n    " << channel << ": ";
        bool first = true;
        for (const auto& pixel : entry.time_series)
        {
            if (!first)
                stream << ", ";
            first = false;
            stream << pixel[channel];
        }
    }
    return stream;
}

std::array<Pixel, num_time_steps> fill_gaps_nearest_copy(const std::array<Pixel, num_time_steps>& pixels)
{
    const auto nearest_non_nan = [](const auto& input, const int i, const Channel channel) -> float
    {
        for (int offset = 0; offset < static_cast<int>(input.size()); ++offset)
        {
            for (const auto index : {i - offset, i + offset})
            {
                if (index >= 0 && index < static_cast<int>(input.size()) && !std::isnan(input.at(index)[channel]))
                {
                    return input.at(index)[channel];
                }
            }
        }
        throw std::runtime_error("Didn't find a single non NaN float ...");
    };
    auto result = pixels;
    for (const auto channel : channels)
    {
        for (int i = 0; i < num_time_steps; ++i)
        {
            result.at(i)[channel] = nearest_non_nan(pixels, i, channel);
        }
    }
    return result;
}

std::vector<DataEntry> load_data(const std::filesystem::path& file_name)
{
    std::vector<DataEntry> result;

    std::ifstream file;
    file.open(file_name);

    std::string line;
    if (!std::getline(file, line, '\n'))
    {
        throw std::runtime_error("File '" + file_name.string() + "' is empty");
    }
    if (line != expected_csv_header)
    {
        throw std::runtime_error("File '" + file_name.string() + "' has unexpected header: " + line);
    }

    while (std::getline(file, line, '\n'))
    {
        if (!line.empty() && line.back() == ',')
        {
            line += "not_a_float";
        }
        std::vector<std::string> cellStrings;
        std::stringstream line_stream(line);
        std::string cell;

        while (std::getline(line_stream, cell, ','))
        {
            cellStrings.push_back(cell);
        }

        if (cellStrings.size() != 2 + num_channels * num_time_steps)
        {
            throw std::runtime_error("File '" + file_name.string() + "' contains unexpected line: " + line);
        }

        DataEntry entry;
        entry.id = std::stoi(cellStrings.at(0));
        entry.target = static_cast<LandClass>(std::stoi(cellStrings.at(1)));

        const std::map<Channel, int> offsets{
            {  Channel::nir,                      2},
            {  Channel::red,     num_time_steps + 2},
            {Channel::green, 2 * num_time_steps + 2}
        };
        for (const auto channel : channels)
        {
            for (int i = 0; i < num_time_steps; ++i)
            {
                const auto& s = cellStrings.at(offsets.at(channel) + i);
                if (s.empty())
                {
                    entry.time_series.at(i)[channel] = std::numeric_limits<double>::quiet_NaN();
                }
                else
                    try
                    {
                        entry.time_series.at(i)[channel] = std::stof(s);
                    }
                    catch (const std::invalid_argument& e)
                    {
                        entry.time_series.at(i)[channel] = std::numeric_limits<double>::quiet_NaN();
                    }
            }
        }

        entry.time_series = fill_gaps_nearest_copy(entry.time_series);

        result.push_back(entry);
    }

    std::cout << "Loaded " << result.size() << " time series" << std::endl;
    file.close();
    return result;
}