#pragma once

#include "common.hpp"

#include <algorithm>
#include <chrono>
#include <iostream>
#include <omp.h>
#include <random>
#include <thread>
#include <torch/torch.h>

struct Bounds
{
    torch::Tensor min, max;
};

Bounds get_bounds(const torch::Tensor& tensor)
{
    Bounds bounds;

    doAssert(tensor.size(0) > 0);
    bounds.min = tensor[0];
    bounds.max = tensor[0];
    for (int i = 1; i < tensor.size(0); ++i)
    {
        bounds.min = torch::minimum(bounds.min, tensor[i]);
        bounds.max = torch::maximum(bounds.max, tensor[i]);
    }
    return bounds;
}

void normalize(torch::Tensor& tensor, const Bounds& bounds)
{
    auto diff = bounds.max - bounds.min;
    diff = torch::where(diff == 0.0f, torch::full(diff.sizes(), 0.00001f), diff);
    for (int i = 0; i < tensor.size(0); ++i)
    {
        tensor[i] = torch::div(tensor[i] - bounds.min, diff);
    }
}

class SatelliteDataset : public torch::data::datasets::Dataset<SatelliteDataset>
{
    torch::Tensor m_input;
    torch::Tensor m_target;
    std::vector<int> m_ids;

  public:
    SatelliteDataset(const std::vector<DataEntry>& data)
    {
        m_input = torch::zeros({static_cast<int>(data.size()), num_channels, num_time_steps});
        m_target = torch::zeros({static_cast<int>(data.size())}, at::TensorOptions(torch::Dtype::Long));
        m_ids = std::vector<int>(data.size());

#pragma omp parallel for
        for (int i = 0; i < static_cast<int>(data.size()); ++i)
        {
            m_input[i] = toTensor(data.at(i).time_series);

            m_target[i] = static_cast<int>(data.at(i).target);
            m_ids.at(i) = data.at(i).id;
        }
    }

    Bounds get_bounds() { return ::get_bounds(m_input); }

    void normalize(const Bounds& bounds) { ::normalize(m_input, bounds); }

    SatelliteDataset clone()
    {
        auto result = *this;
        result.m_input = this->m_input.clone();
        result.m_target = this->m_target.clone();
        return result;
    }

    int getId(size_t index) { return m_ids.at(index); }

    torch::data::Example<> get(size_t index) override { return {m_input[index], m_target[index]}; }

    torch::optional<size_t> size() const override { return m_input.size(0); }
};