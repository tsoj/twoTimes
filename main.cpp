#include "dataset.hpp"
#include "model.hpp"
#include "utils.hpp"

#include <random>

constexpr float train_validation_data_ratio = 0.99;
constexpr int num_total_batches = 1'450'000;
constexpr int batch_size = 32;
constexpr float lr_begin = 0.1;
constexpr float lr_end = 0.03;
constexpr int num_models = 200;
constexpr float max_rand_multiplier = 1.2;

constexpr int num_model_checkpoints = 20;
const int max_num_threads = std::thread::hardware_concurrency() - 1;

std::pair<std::vector<DataEntry>, std::vector<DataEntry>> load_train_validation_sets(const std::filesystem::path& file_name)
{
    auto data = load_data(file_name);

    std::shuffle(data.begin(), data.end(), std::default_random_engine{});

    const int divide_index = static_cast<int>(static_cast<float>(data.size()) * train_validation_data_ratio);

    const auto train_set = std::vector(data.begin(), data.begin() + divide_index);
    const auto validation_set = std::vector(data.begin() + divide_index + 1, data.end());

    return {train_set, validation_set};
}

template<typename EvalF>
float accuracy(SatelliteDataset validation_set, EvalF eval)
{
    float result = 0.0f;
    for (size_t i = 0; i < validation_set.size().value(); ++i)
    {
        const auto example = validation_set.get(i);
        const LandClass output = eval(example.data);
        if (output == static_cast<LandClass>(example.target.item().toInt()))
        {
            result += 1.0f;
        }
    }
    result /= static_cast<float>(validation_set.size().value());
    return result;
}

LandClass get_prediction(Net& model, torch::Tensor input)
{
    model.eval();
    return static_cast<LandClass>(model.forward(input.unsqueeze(0)).reshape({num_classes}).argmax().item().toInt());
}

LandClass get_prediction(std::vector<Net>& models, torch::Tensor input)
{
    auto accumulated_output = torch::zeros({num_classes});
    for (auto& model : models)
    {
        model.eval();
        accumulated_output += model.forward(input.unsqueeze(0)).reshape({num_classes});
    }
    return static_cast<LandClass>(accumulated_output.argmax().item().toInt());
}

float accuracy(SatelliteDataset validation_set, Net& model)
{
    return accuracy(validation_set, [&](torch::Tensor input) { return get_prediction(model, input); });
}

float accuracy(SatelliteDataset validation_set, std::vector<Net>& models)
{
    return accuracy(validation_set, [&](torch::Tensor input) { return get_prediction(models, input); });
}

template<typename Model>
void create_test_results_file(Model& model, const std::string& description, const Bounds& bounds, int checkpoint_index, float validation_accy)
{
    const std::string results_file_path = "results/" + description;
    const std::string results_file_name = results_file_path + "/result_" + description + "_" + std::to_string(checkpoint_index) + "_accy" +
                                          std::to_string(static_cast<int>(100 * validation_accy)) + ".csv";
    std::filesystem::create_directories(results_file_path);
    // creating results for test data
    torch::set_num_threads(max_num_threads);
    auto test_set = SatelliteDataset(load_data("./phase2/SITS-test-data-phase2-nolabel.csv"));
    test_set.normalize(bounds);
    torch::set_num_threads(1);

    std::ofstream result_file;
    result_file.open(results_file_name);
    result_file << "ID,PREDICTED\n";
    for (size_t i = 0; i < test_set.size().value(); ++i)
    {
        auto example = test_set.get(i);
        doAssert(static_cast<LandClass>(example.target.item().toInt()) == LandClass::unknown);
        const LandClass prediction = get_prediction(model, example.data);
        result_file << test_set.getId(i) << "," << static_cast<int>(prediction) << std::endl;
    }
    result_file.close();
}

void train_model(Net& model, SatelliteDataset train_satellite_set, SatelliteDataset validation_satellite_set)
{
    auto train_dataset = train_satellite_set.map(torch::data::transforms::Stack<>());
    const int train_dataset_size = static_cast<int>(train_dataset.size().value());
    auto data_loader = torch::data::make_data_loader(std::move(train_dataset), torch::data::DataLoaderOptions().batch_size(batch_size).workers(1));

    const int num_epochs = (num_total_batches * batch_size) / train_dataset_size;

    auto optimizer = torch::optim::SGD(model.parameters(), torch::optim::SGDOptions(lr_begin));
    const float decay = std::pow(lr_end / lr_begin, 1.0f / static_cast<float>(num_epochs));
    auto scheduler = torch::optim::StepLR(optimizer, 1, decay);

    for (int epoch = 1; epoch <= num_epochs; ++epoch)
    {
        const auto start = std::chrono::steady_clock::now();
        model.train(true);
        for (torch::data::Example<>& batch : *data_loader)
        {
            model.zero_grad();
            const torch::Tensor output = model.forward(batch.data);
            torch::Tensor loss = torch::nll_loss(output, batch.target);
            loss.backward();
            optimizer.step();
        }

        const auto end = std::chrono::steady_clock::now();
        float current_lr = 0.0;
        for (const auto& param_group : optimizer.param_groups())
        {
            if (param_group.has_options())
            {
                current_lr = param_group.options().get_lr();
                break;
            }
        }

        const auto accy = accuracy(validation_satellite_set, model);

        std::cout << "----------------------" << std::endl;
        std::cout << "Epoch " << epoch << "/" << num_epochs << std::endl;
        printf("Accuracy: %0.1f%\n", accy * 100.0);
        printf("Learning rate: %0.4f\n", current_lr);
        std::cout << std::chrono::duration_cast<std::chrono::seconds>(end - start).count() << " s" << std::endl;

        scheduler.step();
    }
}

int main(int argc, char* argv[])
{
    const auto [layers, layer_description] = [&]()
    {
        std::vector<int> lyrs;
        for (int i = 1; i < argc; ++i)
        {
            lyrs.push_back(std::stoi(argv[i]));
        }
        if (lyrs.back() != num_classes)
        {
            throw std::runtime_error("Last layer must be " + std::to_string(num_classes));
        }

        std::string lyr_description = "";
        for (const auto& i : lyrs)
        {
            lyr_description += std::to_string(i) + "-";
        }
        doAssert(lyr_description.size() >= 1 && lyr_description.back() == '-');
        lyr_description.resize(lyr_description.size() - 1);

        return std::pair{lyrs, lyr_description};
    }();

    const auto [train_set, validation_set] = load_train_validation_sets("./phase2/SITS-train-phase2-subset-1000000.csv");

    torch::set_num_threads(max_num_threads);
    auto train_satellite_set = SatelliteDataset(train_set);
    auto validation_satellite_set = SatelliteDataset(validation_set);

    const auto bounds = train_satellite_set.get_bounds();

    train_satellite_set.normalize(bounds);
    validation_satellite_set.normalize(bounds);

    torch::set_num_threads(1);

    std::vector<Net> models;

    std::random_device rd;
    std::mt19937 mt(rd());
    doAssert(max_rand_multiplier >= 1.0f);
    std::uniform_real_distribution<> dist(1.0f / max_rand_multiplier, max_rand_multiplier);

    for (int i = 0; i < num_models; ++i)
    {
        std::vector<int> rand_layers = layers;
        doAssert(rand_layers.back() == num_classes);
        for (int l = 1; l < static_cast<int>(rand_layers.size()) - 1; l++)
        {
            rand_layers[l] *= dist(mt);
        }
        models.emplace_back(rand_layers);
    }

    const std::string description = "multipleModelsRandomLyrs" + layer_description;

    std::cout << "Starting training for " << description << std::endl;

    std::vector<std::thread> threads;

    for (auto& model : models)
    {
        if (static_cast<int>(threads.size()) >= max_num_threads)
        {
            for (auto& thread : threads)
            {
                thread.join();
            }
            threads.clear();
        }
        threads.emplace_back(train_model, std::ref(model), train_satellite_set.clone(), validation_satellite_set.clone());
    }
    for (auto& thread : threads)
    {
        thread.join();
    }

    std::cout << "Finished training" << std::endl;

    for (int current_num_models = 1; current_num_models <= num_models; current_num_models += (num_models / num_model_checkpoints))
    {
        doAssert(models.size() == num_models);
        std::vector<Net> some_models(models.begin(), models.begin() + current_num_models);
        doAssert(static_cast<int>(some_models.size()) == current_num_models);

        // final validation
        const auto accy = accuracy(validation_satellite_set, some_models);
        printf("Final Accuracy: %0.1f%\n", accy * 100.0);

        create_test_results_file(some_models, description, bounds, current_num_models, accy);
    }
}