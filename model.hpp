#pragma once

#include "common.hpp"

#include <algorithm>
#include <chrono>
#include <iostream>
#include <omp.h>
#include <random>
#include <thread>
#include <torch/torch.h>

template<typename Module>
std::vector<int64_t> out_shape_no_batch(Module& module, const std::vector<int64_t>& in_shape_no_batch)
{
    auto x = torch::zeros(in_shape_no_batch);
    x = torch::unsqueeze(x, 0);
    doAssert(x.sizes().size() == in_shape_no_batch.size() + 1);
    x = module(x);
    x = torch::squeeze(x, 0);
    return x.sizes().vec();
}

int num_elements(const std::vector<int64_t>& shape)
{
    int result = 1;
    for (const auto i : shape)
    {
        result *= i;
    }
    return result;
}

struct Net : torch::nn::Module
{
    constexpr static inline float dropout_prob = 0.1;

    Net(std::vector<int> num_neurons)
    {
        constexpr int num_output_channels = 8;
        constexpr int kernel_size = 5;
        cv1_dilution_1 =
            register_module("cv1_dilution_1", torch::nn::Conv1d(torch::nn::Conv1dOptions(num_channels, num_output_channels, kernel_size).dilation(1)));
        cv1_dilution_4 =
            register_module("cv1_dilution_4", torch::nn::Conv1d(torch::nn::Conv1dOptions(num_channels, num_output_channels, kernel_size).dilation(4)));
        cv1_dilution_10 =
            register_module("cv1_dilution_10", torch::nn::Conv1d(torch::nn::Conv1dOptions(num_channels, num_output_channels, kernel_size).dilation(10)));

        auto example_cv_output = forward_only_conv_layers(torch::zeros({1, num_channels, num_time_steps}));

        num_neurons.insert(num_neurons.begin(), example_cv_output.size(1));

        // std::cout << num_neurons.front() << std::endl;
        // std::abort();

        for (size_t i = 1; i < num_neurons.size(); ++i)
        {
            fc_layers.push_back(
                {register_module("fc" + std::to_string(i), torch::nn::Linear(num_neurons.at(i - 1), num_neurons.at(i))),
                 register_module("bn" + std::to_string(i), torch::nn::BatchNorm1d(num_neurons.at(i))),
                 register_module("do" + std::to_string(i), torch::nn::Dropout(dropout_prob))});
        }
    }

    torch::Tensor forward_only_conv_layers(torch::Tensor x)
    {
        const auto batch_size = x.size(0);
        auto od1 = cv1_dilution_1(x).flatten(1);
        auto od4 = cv1_dilution_4(x).flatten(1);
        auto od10 = cv1_dilution_10(x).flatten(1);
        x = torch::cat({od1, od4, od10}, 1);
        doAssert(x.size(0) == batch_size);
        doAssert(x.sizes().size() == 2);
        return x;
    }

    torch::Tensor forward(torch::Tensor x)
    {
        doAssert(x.sizes().size() == 3);
        doAssert(x.size(1) == num_channels);
        doAssert(x.size(2) == num_time_steps);

        x = forward_only_conv_layers(x);
        for (size_t i = 0; i < fc_layers.size(); ++i)
        {
            x = fc_layers.at(i).linear(x);
            if (i < fc_layers.size() - 1)
            {
                x = torch::leaky_relu(x);
                x = fc_layers.at(i).batch_norm(x);
                if (i < fc_layers.size() - 2)
                {
                    x = fc_layers.at(i).dropout(x);
                }
            }
            else
            {
                x = torch::log_softmax(x, 1);
            }
        }
        return x;
    }

    struct FcLayer
    {
        torch::nn::Linear linear;
        torch::nn::BatchNorm1d batch_norm;
        torch::nn::Dropout dropout;
    };

    std::vector<FcLayer> fc_layers;

    torch::nn::Conv1d cv1_dilution_1{nullptr};
    torch::nn::Conv1d cv1_dilution_4{nullptr};
    torch::nn::Conv1d cv1_dilution_10{nullptr};
};
