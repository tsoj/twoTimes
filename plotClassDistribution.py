
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

data = [80014,162150,63515,63995,106179,4762,135868,44904,75325,54061,6744,11190,5508,25015,7963,12895,7225,1001,22554,2007,58,88037,6120,2910]
labels = ["prairie temporaire", "ble", "pre", "feuillus", "tournesol", "mais ensillage", "jachere", "bati dense", "bati diffus", "friche", "resineux", "sorgho", "pois", "orge", "bati indu", "soja", "eau", "eucalyptus", "colza", "lac", "peupliers", "mais", "graviere", "surface minerale"]

# Create a figure and axis object
fig, ax = plt.subplots()

# Create a bar chart
ax.bar(labels, data)

# Set the axis labels
ax.set_ylabel('Count')


# Format the y-axis labels with a thousands separator
fmt = '{x:,.0f}'
tick = mtick.StrMethodFormatter(fmt)
ax.yaxis.set_major_formatter(tick)

# Display the plot
plt.xticks(rotation=90)
plt.tight_layout()
plt.savefig('classDistribution.png', format='png', dpi=400.0, transparent=True)
plt.show()