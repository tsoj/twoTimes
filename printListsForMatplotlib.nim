import std/[
    json
]


let jsonNode = readFile("training_info_simple_layers_2million_steps_batchNorm.json").parseJson

for modelDescription, node in jsonNode:
    var values: seq[float]
    for elem in node:
        # first (private) value has 40% of all samples, the rest are public
        values.add 0.4*elem[0].getFloat + 0.6*elem[1].getFloat

    echo modelDescription, ": ", values