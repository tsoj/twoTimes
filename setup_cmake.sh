#!/bin/bash

rm -rf ./Makefile ./CMakeFiles ./CMakeCache.txt ./cmake_install.cmake

if [ ! -d "./libtorch" ]; then
    rm libtorch-shared-with-deps-latest.zip
    wget https://download.pytorch.org/libtorch/nightly/cpu/libtorch-shared-with-deps-latest.zip
    unzip libtorch-shared-with-deps-latest.zip
    rm libtorch-shared-with-deps-latest.zip
fi


export CC=/usr/bin/clang
export CXX=/usr/bin/clang++

cmake -DCMAKE_PREFIX_PATH=$(pwd)/libtorch .