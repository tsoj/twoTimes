#include <iostream>

#define doAssert(assertion)                                                                                                                                    \
    if (!(assertion))                                                                                                                                          \
    {                                                                                                                                                          \
        std::cerr << __FILE__ << ":" << __LINE__ << ": "                                                                                                       \
                  << "Assertion `" << #assertion << "' failed." << std::endl;                                                                                  \
        std::abort();                                                                                                                                          \
    }
